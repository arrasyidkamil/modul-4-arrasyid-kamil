package com.example.fiana.sansacafee;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class MainMenu extends AppCompatActivity {
    private static final String TAG = "MainMenu";
    RecyclerView recycleView;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter<Items, ItemsHolder> adapter;
    private Button button_load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tambah = new Intent(MainMenu.this, Inputmenu.class);
                startActivity(tambah);
            }
        });

        recycleView = findViewById(R.id.terserah);
        recycleView.setLayoutManager (new LinearLayoutManager(this));

        FirebaseFirestore rootRef = FirebaseFirestore.getInstance();
        Query query = rootRef.collection("items");

        FirestoreRecyclerOptions<Items> options = new FirestoreRecyclerOptions.Builder<Items>().setQuery(query, Items.class).build();

        adapter = new FirestoreRecyclerAdapter<Items, ItemsHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull ItemsHolder holder, int position, @NonNull Items model) {
                holder.setNama(model.getNama());
                holder.setHarga(model.getHarga() );
//                holder.setNama("test");
//                holder.setHarga("25000");
            }
            @NonNull
            @Override
            public ItemsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
                return new ItemsHolder(view);
            }
        };
        button_load = findViewById(R.id.btn);
        button_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleView.setAdapter(adapter);
            }
        });

    }

    public class ItemsHolder extends RecyclerView.ViewHolder {
        private View view;

        ItemsHolder(View itemView){
            super(itemView);
            view = itemView;
        }

        void setNama(String nama){
            TextView textView = view.findViewById(R.id.et_nama);
            textView.setText(nama);
        }

        void setHarga(String harga){
            TextView textView = view.findViewById(R.id.et_harga);
            textView.setText(harga);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }
    }

