package com.example.fiana.sansacafee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private EditText Emaill, Passwordd;
    private Button btn_login;
    private static final  String TAG = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        Emaill = findViewById(R.id.Email);
        Passwordd = findViewById(R.id.Password);
        btn_login = findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email,password;
                email = Emaill.getText().toString();
                password = Passwordd.getText().toString();

                mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent mainmenu = new Intent(Login.this, MainMenu.class);
                            startActivity(mainmenu);
                            finish();
                        }else{
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
            }
        });

    }


    public void okeoke(View view) {
        Intent Register= new Intent(Login.this, Registrasi.class);
        startActivity(Register);
    }

}
