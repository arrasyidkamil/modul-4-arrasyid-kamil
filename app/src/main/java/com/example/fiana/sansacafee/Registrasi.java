package com.example.fiana.sansacafee;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class Registrasi extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button button_register;
    private EditText text_nama, text_email, text_password;
    private static final String TAG = "register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        mAuth = FirebaseAuth.getInstance();
        button_register = findViewById(R.id.btn);
        text_nama = findViewById(R.id.nama);
        text_email = findViewById(R.id.Email);
        text_password = findViewById(R.id.password);

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email, password;
                email = text_email.getText().toString();
                password = text_password.getText().toString();
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent mainmenu = new Intent(Registrasi.this, Login.class);
                            startActivity(mainmenu);
                            finish();
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
            }
        });
    }
    private void registerUser() {
        String email = text_email.getText().toString().trim();
        String password = text_password.getText().toString().trim();

        if (email.isEmpty()) {
            text_email.setError("Email is required");
            text_email.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            text_email.setError("Please enter a valid email");
            text_email.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            text_password.setError("Password is required");
            text_password.requestFocus();
            return;
        }

        if (password.length() < 6) {
            text_password.setError("Minimum lenght of password should be 6");
            text_password.requestFocus();
            return;
        }


        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    finish();
                    startActivity(new Intent(Registrasi.this, MainActivity.class));
                } else {

                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(getApplicationContext(), "You are already registered", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    public void okeokeoke(View view) {
        Intent masuk = new Intent(Registrasi.this, Login.class);
        startActivity(masuk);
    }

    public void regis(View view) {
        Intent regiss = new Intent(Registrasi.this, Login.class);
        startActivity(regiss);
    }
}
